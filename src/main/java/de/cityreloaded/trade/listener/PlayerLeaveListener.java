package de.cityreloaded.trade.listener;

import de.cityreloaded.trade.Trade;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Lars on 12.11.2016.
 */
public class PlayerLeaveListener implements Listener {

  @EventHandler
  public void onPlayerLeave(PlayerQuitEvent playerQuitEvent) {
    String message = String.format("%s ist nun offline.", playerQuitEvent.getPlayer().getDisplayName());

    Trade.getInstance().getTradeManager().getTradeOffers().forEach(tradeOffer -> {
      if (tradeOffer.getSender() == playerQuitEvent.getPlayer()) {
        tradeOffer.getRecipient().sendMessage(message);
        tradeOffer.cancel();
      }

      if (tradeOffer.getRecipient() == playerQuitEvent.getPlayer()) {
        tradeOffer.getSender().sendMessage(message);
        tradeOffer.cancel();
      }
    });
  }

}
