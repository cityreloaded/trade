package de.cityreloaded.trade.commands;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.11.2016.
 */
public class CancelCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "cancel";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"trade"};
  }

  @Override
  public String getHelp() {
    return "Bricht einen Handel ab";
  }

  @Override
  public String getSyntax() {
    return "cancel [spieler]";
  }

  @Override
  public String getPermission() {
    return "Trade.Cancel";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 1) {
      Player sender = Bukkit.getPlayer(args[0]);

      if (sender != null) {
        TradeOffer tradeOffer = Trade.getInstance().getTradeManager().getPendingTrade(commandSender.getName());
        if (tradeOffer == null) {
          tradeOffer = this.getTradeOffer(sender.getName(), commandSender.getName());
        }

        if (tradeOffer != null) {
          String playerName = tradeOffer.getSender().getName().equals(commandSender.getName()) ?
                  tradeOffer.getRecipient().getDisplayName() : tradeOffer.getSender().getDisplayName();

          if (!tradeOffer.isCompleted() && tradeOffer.isSent() && !tradeOffer.isTransferred()) {
            tradeOffer.cancel();
            this.sendMessage(commandSender, String.format("Der Handel mit %s wurde abgebrochen.", playerName));
          } else {
            this.sendMessage(commandSender, String.format("Du hast keinen gültigen Handel mit %s.", playerName));
          }
        } else {
          this.sendMessage(commandSender, String.format("Du hast keinen Handel mit %s.", sender.getDisplayName()));
        }
      } else {
        this.sendMessage(commandSender, String.format("%s ist nicht online.", args[0]));
      }

      return true;
    }

    return false;
  }

  private TradeOffer getTradeOffer(String senderName, String recipientName) {
    for (TradeOffer tradeOffer : Trade.getInstance().getTradeManager().getPendingOffers(recipientName)) {
      if (tradeOffer.getSender().getName().equals(senderName)) {
        return tradeOffer;
      }
    }

    return null;
  }

}
