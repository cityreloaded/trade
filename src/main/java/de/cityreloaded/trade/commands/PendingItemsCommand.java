package de.cityreloaded.trade.commands;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.util.PlayerUtils;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Lars on 13.11.2016.
 */
public class PendingItemsCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "pendingitems";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"trade"};
  }

  @Override
  public String[] getAliases() {
    return new String[]{"items"};
  }

  @Override
  public String getHelp() {
    return "Gibt dir Items zurück, die nicht übertragen werden konnten.";
  }

  @Override
  public String getPermission() {
    return "Trade.PendingItems";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());

    ItemStack[] pendingItems = Trade.getInstance().getTradeManager().getPendingItems(player.getName());
    Trade.getInstance().getTradeManager().clearPendingItems(player.getName());

    if (pendingItems.length > 0) {
      PlayerUtils.sendItems(player, pendingItems);

      if (Trade.getInstance().getTradeManager().getPendingItems(player.getName()).length == 0) {
        this.sendMessage(commandSender, "Deine Items wurden übertragen.");
      }
    } else {
      this.sendMessage(commandSender, "Du hast keine unübertragenen Items.");
    }

    return true;
  }
}
