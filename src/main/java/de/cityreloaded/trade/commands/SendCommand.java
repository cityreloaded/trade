package de.cityreloaded.trade.commands;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Lars on 12.11.2016.
 */
public class SendCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "send";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"trade"};
  }

  @Override
  public String getHelp() {
    return "Sendet einen Handel mit dem [betrag] an [spieler]. Es wird der Gegenstand angeboten, den du in der Hand hälst.";
  }

  @Override
  public String getSyntax() {
    return "send [spieler] [betrag]";
  }

  @Override
  public String getPermission() {
    return "Trade.Send";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player sender = Bukkit.getPlayer(commandSender.getName());

    if (Trade.getInstance().getBlockedWorlds().contains(sender.getWorld().getName())) {
      this.sendMessage(commandSender, "Handel ist in dieser Welt deaktiviert.");

      return true;
    }

    if (args.length >= 2) {
      String playerName = args[0];
      int price;

      try {
        price = Integer.parseInt(args[1]);
      } catch (NumberFormatException e) {
        this.sendMessage(commandSender, String.format("%s ist kein Gültiger Betrag. Es werden nur ganze Zahlen erlaubt.", args[1]));

        return true;
      }

      Player recipient = Bukkit.getPlayer(playerName);
      ItemStack currentItemStack = sender.getItemInHand();

      if (sender == recipient) {
        this.sendMessage(commandSender, "Du kannst nicht mit dir selbst handeln!");

        return true;
      }

      if (!Trade.getInstance().getTradeManager().hasPendingTrade(sender.getName())) {
        if (currentItemStack != null && currentItemStack.getType() != Material.AIR) {
          if (recipient != null) {
            TradeOffer tradeOffer = new TradeOffer(sender, recipient, price);
            tradeOffer.addItemStack(currentItemStack);

            tradeOffer.send();
            this.sendMessage(commandSender, "Das Angebot wurde gesendet.");
          } else {
            this.sendMessage(commandSender, String.format("%s ist zurzeit nicht online!", playerName));
          }
        } else {
          this.sendMessage(commandSender, "Du hast keinen Gegenstand in der Hand.");
        }
      } else {
        TradeOffer tradeOffer = Trade.getInstance().getTradeManager().getPendingTrade(sender.getName());
        this.sendMessage(commandSender, "Du hast bereits ein Handelsangebot gesendet.");
        this.sendMessage(commandSender, String.format("Du kannst es mit \"/trade cancel %s\" abbrechen.", tradeOffer.getRecipient().getName()));
      }

      return true;
    }

    return false;
  }
}
