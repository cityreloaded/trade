package de.cityreloaded.trade.commands;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.11.2016.
 */
public class InspectCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "inspect";
  }

  @Override
  public String[] getAliases() {
    return new String[]{"open", "view"};
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"trade"};
  }

  @Override
  public String getHelp() {
    return "Öffnet einen Handel";
  }

  @Override
  public String getSyntax() {
    return "inspect [spieler]";
  }

  @Override
  public String getPermission() {
    return "Trade.Inspect";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());

    if (Trade.getInstance().getBlockedWorlds().contains(player.getWorld().getName())) {
      this.sendMessage(commandSender, "Handel ist in dieser Welt deaktiviert.");

      return true;
    }

    if (args.length >= 1) {
      Player sender = Bukkit.getPlayer(args[0]);

      if (sender != null) {
        TradeOffer tradeOffer = this.getTradeOffer(sender.getName(), commandSender.getName());

        if (tradeOffer != null) {
          if (!tradeOffer.isCompleted() && tradeOffer.isSent() && !tradeOffer.isTransferred()) {
            tradeOffer.buildGui().sendToPlayer(tradeOffer.getRecipient());
            this.sendMessage(commandSender, String.format("Öffne Handelsangebot von %s.", tradeOffer.getSender().getDisplayName()));
          } else {
            // should actually never happen
          }
        } else {
          this.sendMessage(commandSender, String.format("Du hast kein offenes Handelsangebot von %s.", sender.getDisplayName()));
        }
      } else {
        this.sendMessage(commandSender, String.format("%s ist nicht online.", args[0]));
      }

      return true;
    }

    return false;
  }

  private TradeOffer getTradeOffer(String senderName, String recipientName) {
    for (TradeOffer tradeOffer : Trade.getInstance().getTradeManager().getPendingOffers(recipientName)) {
      if (tradeOffer.getSender().getName().equals(senderName)) {
        return tradeOffer;
      }
    }

    return null;
  }
}
