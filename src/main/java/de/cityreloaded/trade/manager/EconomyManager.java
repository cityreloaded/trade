package de.cityreloaded.trade.manager;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.RegisteredServiceProvider;

import lombok.Getter;

/**
 * Created by Lars on 12.11.2016.
 */
public class EconomyManager {
  @Getter
  private Economy economy;

  public boolean setupEconomy() {
    if (!Bukkit.getServer().getPluginManager().isPluginEnabled("Vault"))
      return false;

    RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);

    if (rsp == null) {
      return false;
    }

    this.economy = rsp.getProvider();

    return economy != null;
  }

  public boolean hasEnough(OfflinePlayer offlinePlayer, double money) {
    return this.economy.has(offlinePlayer, money);
  }

  public boolean subtractMoney(OfflinePlayer offlinePlayer, double money) {
    if (money < 0) {
      return addMoney(offlinePlayer, -money);
    } else if (money > 0) {
      EconomyResponse economyResponse = this.economy.withdrawPlayer(offlinePlayer, money);

      return economyResponse.type == EconomyResponse.ResponseType.SUCCESS;
    } else {
      return true;
    }
  }

  public boolean addMoney(OfflinePlayer offlinePlayer, double money) {
    if (money < 0) {
      return subtractMoney(offlinePlayer, -money);
    } else if (money > 0) {
      EconomyResponse economyResponse = this.economy.depositPlayer(offlinePlayer, money);

      return economyResponse.type == EconomyResponse.ResponseType.SUCCESS;
    } else {
      return true;
    }
  }

  public boolean transferMoney(OfflinePlayer sender, OfflinePlayer receiver, double money) {
    if (this.hasEnough(sender, money)) {
      this.subtractMoney(sender, money);
      this.addMoney(receiver, money);

      return true;
    }

    return false;
  }
}
