package de.cityreloaded.trade.manager;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Lars on 12.11.2016.
 */
public class TradeManager {
  private ConcurrentHashMap<String, ItemStack[]> pendingItems;
  private ConcurrentHashMap<String, TradeOffer> pendingTrades;

  public TradeManager() {
    this.pendingItems = new ConcurrentHashMap<>();
    this.pendingTrades = new ConcurrentHashMap<>();
  }

  public boolean sendOffer(TradeOffer tradeOffer) {
    String prefix = Trade.getInstance().getCommandApi().getChatPrefix();
    if (this.registerTrade(tradeOffer)) {
      if (tradeOffer.collectItems()) {

        tradeOffer.getSender().sendMessage(prefix + String.format("Dein Angebot wurde an %s versendet ($%d).", tradeOffer.getRecipient().getDisplayName(), tradeOffer.getPrice()));
        tradeOffer.getSender().sendMessage(prefix + String.format("Benutze \"/trade cancel %s\", um den Handel abzubrechen.", tradeOffer.getRecipient().getName()));

        tradeOffer.getRecipient().sendMessage(prefix + String.format("%s hat dir ein Handelsangebot gesendet.", tradeOffer.getSender().getDisplayName()));
        tradeOffer.getRecipient().sendMessage(prefix + String.format("Benutze \"/trade inspect %s\", um den Handel anzusehen.", tradeOffer.getSender().getName()));
        tradeOffer.setSent(true);

        Bukkit.getScheduler().runTaskLater(Trade.getInstance(), () -> {
          if (!tradeOffer.isCompleted() && this.pendingTrades.containsValue(tradeOffer)) {
            tradeOffer.cancel();
          }
        }, 20 * 60);
      } else {
        tradeOffer.getSender().sendMessage(prefix + "Deine Items konnten nicht aus deinem Inventar genommen werden. Versuche es nochmal!");
        tradeOffer.cancel();
      }
    }

    return false;
  }

  public boolean cancelOffer(TradeOffer tradeOffer) {
    if (this.pendingTrades.containsValue(tradeOffer) && !tradeOffer.isCompleted()) {
      tradeOffer.resetItems();
      String prefix = Trade.getInstance().getCommandApi().getChatPrefix();
      tradeOffer.getSender().sendMessage(prefix + "Der Handel wurde abgebrochen. Du erhälst deine Items zurück.");
      tradeOffer.getRecipient().sendMessage(prefix + "Der Handel wurde abgebrochen. Dein Kontostand bleibt unverändert.");

      this.pendingTrades.remove(tradeOffer.getSender().getName());

      return true;
    }

    return false;
  }

  public boolean completeOffer(TradeOffer tradeOffer) {
    if (this.pendingTrades.containsValue(tradeOffer)) {
      EconomyManager economyManager = Trade.getInstance().getEconomyManager();
      String prefix = Trade.getInstance().getCommandApi().getChatPrefix();

      if (economyManager.transferMoney(tradeOffer.getRecipient(), tradeOffer.getSender(), tradeOffer.getPrice())) {
        tradeOffer.getSender().sendMessage(prefix + String.format("Der Handel wurde abgeschlossen. Dir wurden $%d gutgeschrieben.", tradeOffer.getPrice()));
        if (Trade.getInstance().getTradeManager().getPendingItems(tradeOffer.getRecipient().getName()).length == 0) {
          tradeOffer.getRecipient().sendMessage(prefix + "Der Handel wurde abgeschlossen. Die Gegenstände befinden sich in deinem Inventar.");
        }
        tradeOffer.setCompleted(true);

        this.pendingTrades.remove(tradeOffer.getSender().getName());
      } else {
        tradeOffer.getRecipient().sendMessage(prefix + "Du hast nicht genügend Geld!");
        tradeOffer.cancel();
      }

      return true;
    }

    return false;
  }

  private boolean registerTrade(TradeOffer tradeOffer) {
    if (!this.hasPendingTrade(tradeOffer.getSender().getName())) {
      this.pendingTrades.put(tradeOffer.getSender().getName(), tradeOffer);
      return true;
    }

    return false;
  }

  public boolean hasPendingTrade(String playerName) {
    return this.pendingTrades.containsKey(playerName);
  }

  public TradeOffer getPendingTrade(String playerName) {
    return this.pendingTrades.get(playerName);
  }

  public boolean hasPendingOffer(String playerName) {
    return this.getPendingOffers(playerName).length > 0;
  }

  public TradeOffer[] getPendingOffers(String playerName) {
    return this.pendingTrades.values().stream().filter(
            tradeOffer -> tradeOffer.getRecipient().getName().equals(playerName)
    ).toArray(TradeOffer[]::new);
  }

  public TradeOffer getById(String id) {
    UUID uuid = UUID.fromString(id);
    for (TradeOffer tradeOffer : this.pendingTrades.values()) {
      if (tradeOffer.getId().equals(uuid)) {
        return tradeOffer;
      }
    }

    return null;
  }

  public ItemStack[] getPendingItems(String playerName) {
    return this.pendingItems.getOrDefault(playerName, new ItemStack[0]);
  }

  public void clearPendingItems(String playerName) {
    this.pendingItems.remove(playerName);
  }

  public void addPendingItems(String playerName, ItemStack[] itemStacks) {
    ItemStack[] pendingItems = this.getPendingItems(playerName);
    ArrayList<ItemStack> pendingItemStacks = new ArrayList<>(Arrays.asList(pendingItems));
    pendingItemStacks.addAll(Arrays.asList(itemStacks));
    this.pendingItems.put(playerName, pendingItemStacks.toArray(new ItemStack[pendingItemStacks.size()]));
  }

  public Collection<TradeOffer> getTradeOffers() {
    return this.pendingTrades.values();
  }

}
