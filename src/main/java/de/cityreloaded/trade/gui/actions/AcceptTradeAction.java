package de.cityreloaded.trade.gui.actions;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;
import de.cityreloaded.trade.manager.EconomyManager;
import de.paxii.bukkit.guimanager.GuiProvider;
import de.paxii.bukkit.guimanager.actions.Action;
import de.paxii.bukkit.guimanager.actions.ActionExecutor;

/**
 * Created by Lars on 12.11.2016.
 */
public class AcceptTradeAction extends AbstractTradeAction {
  @Override
  public String getAction() {
    return "accepttrade";
  }

  @Override
  public boolean executeAction(ActionExecutor actionExecutor, String[] args) {
    if (args[0].equalsIgnoreCase("%p")) {
      String uuid = args[1];
      TradeOffer tradeOffer = Trade.getInstance().getTradeManager().getById(uuid);

      if (tradeOffer != null) {
        EconomyManager economyManager = Trade.getInstance().getEconomyManager();
        if (economyManager.hasEnough(tradeOffer.getRecipient(), tradeOffer.getPrice())) {
          if (tradeOffer.transferItems()) {
            Trade.getInstance().getTradeManager().completeOffer(tradeOffer);
          } else {
            // Receiver is offline - Highly unlikely at this point.
          }
        } else {
          tradeOffer.cancel();
          this.sendMessage(actionExecutor, "Du hast nicht genügend Geld!");
        }

        GuiProvider.getINSTANCE().getActionManager().executeAction(new Action("closegui:%p"), tradeOffer.getRecipient());
      } else {
        this.sendMessage(actionExecutor, "Die Handelsanfrage ist nicht mehr gültig!");
      }
    }

    return false;
  }

  @Override
  public int getRequiredArgumentCount() {
    return 2;
  }
}
