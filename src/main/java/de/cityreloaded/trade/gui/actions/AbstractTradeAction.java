package de.cityreloaded.trade.gui.actions;

import de.cityreloaded.trade.Trade;
import de.paxii.bukkit.guimanager.actions.ActionExecutor;
import de.paxii.bukkit.guimanager.actions.IAction;

import org.bukkit.command.CommandSender;

/**
 * Created by Lars on 12.11.2016.
 */
public abstract class AbstractTradeAction implements IAction {
  protected void sendMessage(ActionExecutor actionExecutor, String message) {
    this.sendMessage(actionExecutor.getCommandSender(), message);
  }

  protected void sendMessage(CommandSender commandSender, String message) {
    commandSender.sendMessage(Trade.getInstance().getCommandApi().getChatPrefix() + message);
  }
}
