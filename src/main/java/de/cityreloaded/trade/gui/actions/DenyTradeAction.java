package de.cityreloaded.trade.gui.actions;

import de.cityreloaded.trade.Trade;
import de.cityreloaded.trade.TradeOffer;
import de.paxii.bukkit.guimanager.GuiProvider;
import de.paxii.bukkit.guimanager.actions.Action;
import de.paxii.bukkit.guimanager.actions.ActionExecutor;

/**
 * Created by Lars on 12.11.2016.
 */
public class DenyTradeAction extends AbstractTradeAction {
  @Override
  public String getAction() {
    return "denytrade";
  }

  @Override
  public boolean executeAction(ActionExecutor actionExecutor, String[] args) {
    if (args[0].equalsIgnoreCase("%p")) {
      String uuid = args[1];
      TradeOffer tradeOffer = Trade.getInstance().getTradeManager().getById(uuid);

      if (tradeOffer != null) {
        tradeOffer.cancel();
      } else {
        this.sendMessage(actionExecutor, "Die Handelsanfrage ist nicht mehr gültig!");
      }

      GuiProvider.getINSTANCE().getActionManager().executeAction(new Action("closegui:%p"), actionExecutor.getPlayer());
    }

    return false;
  }

  @Override
  public int getRequiredArgumentCount() {
    return 2;
  }
}
