package de.cityreloaded.trade;

import de.cityreloaded.trade.commands.CancelCommand;
import de.cityreloaded.trade.commands.InspectCommand;
import de.cityreloaded.trade.commands.PendingItemsCommand;
import de.cityreloaded.trade.commands.SendCommand;
import de.cityreloaded.trade.gui.actions.AcceptTradeAction;
import de.cityreloaded.trade.gui.actions.DenyTradeAction;
import de.cityreloaded.trade.listener.PlayerLeaveListener;
import de.cityreloaded.trade.manager.EconomyManager;
import de.cityreloaded.trade.manager.TradeManager;
import de.paxii.bukkit.commandapi.CommandApi;
import de.paxii.bukkit.guimanager.GuiProvider;

import org.bukkit.ChatColor;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

import lombok.Getter;

/**
 * Created by Lars on 12.11.2016.
 */
public class Trade extends JavaPlugin {
  @Getter
  private static Trade instance;

  @Getter
  private CommandApi commandApi;

  @Getter
  private TradeManager tradeManager;

  @Getter
  private EconomyManager economyManager;

  @Override
  public void onEnable() {
    Trade.instance = this;
    this.commandApi = new CommandApi();
    this.economyManager = new EconomyManager();
    this.tradeManager = new TradeManager();
    this.commandApi.setChatPrefix(ChatColor.GOLD + "[Trade] " + ChatColor.RESET);

    if (!this.economyManager.setupEconomy()) {
      System.out.println(this.commandApi.getChatPrefix() + "Could not register Economy");
      this.setEnabled(false);
    }
    this.setupConfig();

    this.getServer().getPluginManager().registerEvents(new PlayerLeaveListener(), this);

    this.commandApi.addCommand(new SendCommand());
    this.commandApi.addCommand(new InspectCommand());
    this.commandApi.addCommand(new CancelCommand());
    this.commandApi.addCommand(new PendingItemsCommand());

    GuiProvider.getINSTANCE().getActionManager().addAction(new AcceptTradeAction());
    GuiProvider.getINSTANCE().getActionManager().addAction(new DenyTradeAction());

    this.getCommand("trade").setExecutor(this.commandApi.getExecutor());
  }

  public List<String> getBlockedWorlds() {
    return this.getConfig().getStringList("blockedWorlds");
  }

  private void setupConfig() {
    this.getConfig().options().copyDefaults(true);
    this.saveConfig();
  }

  @Override
  public void onDisable() {
    this.tradeManager.getTradeOffers().forEach(TradeOffer::cancel);
    PlayerQuitEvent.getHandlerList().unregister(this);
  }
}
