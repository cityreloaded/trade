package de.cityreloaded.trade;

import de.cityreloaded.trade.util.PlayerUtils;
import de.paxii.bukkit.guimanager.actions.Action;
import de.paxii.bukkit.guimanager.gui.PlayerGUI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import lombok.Data;

/**
 * Created by Lars on 12.11.2016.
 */
@Data
public class TradeOffer {
  private final UUID id;
  private Player sender;
  private Player recipient;
  private ArrayList<ItemStack> items;
  private int price;
  private boolean transferred;
  private boolean sent;
  private boolean completed;

  public TradeOffer(Player sender, Player recipient, int price) {
    this.id = UUID.randomUUID();
    this.sender = sender;
    this.recipient = recipient;
    this.price = price;
    this.items = new ArrayList<>();
  }

  public void addItemStack(ItemStack itemStack) {
    // FIXME Sort items by Type (not necessary until we allow multiple itemstacks)
    // Only allow Items on the first 3 rows
    if (this.items.size() < 27) {
      this.items.add(itemStack);
    }
  }

  public void send() {
    Trade.getInstance().getTradeManager().sendOffer(this);
  }

  public void cancel() {
    Trade.getInstance().getTradeManager().cancelOffer(this);
  }

  public boolean transferItems() {
    return this.sendItems(this.recipient);
  }

  public boolean resetItems() {
    return this.sendItems(this.sender);
  }

  public boolean collectItems() {
    boolean result = true;
    ArrayList<ItemStack> transferredItems = new ArrayList<>();
    for (ItemStack itemStack : this.items) {
      int firstSlot = this.sender.getInventory().first(itemStack);
      if (firstSlot != -1) {
        this.sender.getInventory().setItem(firstSlot, new ItemStack(Material.AIR, 1));
        transferredItems.add(itemStack);
      } else {
        result = false;
      }
    }

    if (!result) {
      // restore items
      for (ItemStack transferredItem : transferredItems) {
        this.sender.getInventory().addItem(transferredItem);
      }
    }

    this.sender.updateInventory();

    return result;
  }

  private boolean sendItems(Player player) {
    boolean result = PlayerUtils.sendItems(player, this.items.toArray(new ItemStack[this.items.size()]));

    if (result) {
      this.transferred = true;
    }

    return result;
  }

  public PlayerGUI buildGui() {
    PlayerGUI playerGUI = new PlayerGUI(this.sender.getName(), 6);
    playerGUI.getSettings().setPermanent(false);

    for (ItemStack itemStack : this.items) {
      playerGUI.addItem(this.items.indexOf(itemStack), itemStack);
    }

    ItemStack itemStackAccept = new ItemStack(Material.WOOL, 1);
    itemStackAccept.setDurability((short) 5); // Green Wool
    ItemMeta itemMetaAccept = itemStackAccept.getItemMeta();
    itemMetaAccept.setDisplayName("Handel akzeptieren");
    itemMetaAccept.setLore(Arrays.asList(
            String.format("%s bietet dir diese Gegenstände", this.sender.getDisplayName()),
            String.format("für %d Dollar an.", this.price)
    ));
    itemStackAccept.setItemMeta(itemMetaAccept);
    ItemStack itemStackDeny = new ItemStack(Material.WOOL, 1);
    itemStackDeny.setDurability((short) 14); // Red Wool
    ItemMeta itemMetaDeny = itemStackDeny.getItemMeta();
    itemMetaDeny.setDisplayName("Handel ablehnen");
    itemMetaDeny.setLore(Arrays.asList(
            String.format("%s bietet dir diese Gegenstände", this.sender.getDisplayName()),
            String.format("für %d Dollar an.", this.price)
    ));
    itemStackDeny.setItemMeta(itemMetaDeny);
    playerGUI.addItem(38, itemStackAccept, new Action("accepttrade:%p:" + this.id.toString()));
    playerGUI.addItem(42, itemStackDeny, new Action("denytrade:%p:" + this.id.toString()));

    return playerGUI;
  }

  public boolean equals(TradeOffer tradeOffer) {
    return this.id.equals(tradeOffer.getId());
  }
}
