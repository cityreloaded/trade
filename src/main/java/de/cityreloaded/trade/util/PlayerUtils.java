package de.cityreloaded.trade.util;

import de.cityreloaded.trade.Trade;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by Lars on 13.11.2016.
 */
public class PlayerUtils {

  public static boolean sendItems(Player player, ItemStack[] itemStacks) {
    if (player.isOnline()) {
      ArrayList<ItemStack> notGivenItems = new ArrayList<>(
              player.getInventory().addItem(itemStacks).values()
      );

      if (notGivenItems.size() > 0) {
        Trade.getInstance().getTradeManager().addPendingItems(player.getName(), notGivenItems.toArray(new ItemStack[notGivenItems.size()]));

        String prefix = Trade.getInstance().getCommandApi().getChatPrefix();
        player.sendMessage(prefix + "Deine Items konnten nicht übertragen werden, da dein Inventar voll ist.");
        player.sendMessage(prefix + "Gib \"/trade pendingitems\" ein, nachdem du dein Inventar geleert hast, um sie zu erhalten.");
      }

      player.updateInventory();

      return true;
    }

    return false;
  }

}
